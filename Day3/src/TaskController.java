import java.awt.Point;
import java.util.ArrayList;
import java.util.Scanner;

public class TaskController {
	public static Scanner ip = new Scanner(System.in);
	public static int d=0;
//	public static int count=0;
	public void Add(ArrayList<Task> arr) {
		
		System.out.print("Enter title to add : ");
		String title = ip.nextLine();
		System.out.print("Enter Text to add : ");
		String text = ip.nextLine();
		System.out.print("Enter assignedTo to add : ");
		String assignedTo = ip.nextLine();
		Task a = new Task(d,title,text,assignedTo);
		d++;
		arr.add(a);
	}
	
	public void Print(ArrayList<Task> arr) {
		for(Task a : arr) {
			System.out.println(a);
		}
	}
	
	public void UpdateTitle(ArrayList<Task> arr) {
		int pos = SearchTitle(arr);
		System.out.println("Enter the new Text which you want to add : ");
		String newS = ip.nextLine();
		if(pos>=0) {
			arr.get(pos).setTitle(newS);
		}else {
			System.out.println("No value exists");
		}
	}
	public void UpdateText(ArrayList<Task> arr) {
		int pos = SearchText(arr);
		System.out.println("Enter the new Text which you want to add : ");
		String newS = ip.nextLine();
		if(pos>=0) {
			arr.get(pos).setText(newS);
		}else {
			System.out.println("No value exists");
		}
	}
	public void UpdateAssignedTo(ArrayList<Task> arr) {
		int pos = SearchAssignedTo(arr);
		System.out.println("Enter the new Text which you want to add : ");
		String newS = ip.nextLine();
		if(pos>=0) {
			arr.get(pos).setAssignedTo(newS);
		}else {
			System.out.println("No value exists");
		}
	}
	public int SearchTitle(ArrayList<Task> arr) {
		int pos =-1;
		System.out.print("Enter the Title you want to search: ");
		String search = ip.nextLine();
		for(int i=0;i<arr.size();i++) {
			if(arr.get(i).getTitle().toLowerCase().trim().equals(search.toLowerCase().trim())) {
				pos = i;
				break;
			}
		}
		return pos;
	}
	public int SearchText(ArrayList<Task> arr) {
		int pos =-1;
		System.out.print("Enter the Text you want to search: ");
		String search = ip.nextLine();
		for(int i=0;i<arr.size();i++) {
			if(arr.get(i).getText().toLowerCase().trim().equals(search.toLowerCase().trim())) {
				pos = i;
				break;
			}
		}
		return pos;
	}
	public int SearchAssignedTo(ArrayList<Task> arr) {
		int pos =-1;
		System.out.print("Enter the AssignedTo you want to search: ");
		String search = ip.nextLine();
		for(int i=0;i<arr.size();i++) {
			if(arr.get(i).getAssignedTo().toLowerCase().trim().equals(search.toLowerCase().trim())) {
				pos = i;
				break;
			}
		}
		return pos;
	}
	public void DeleteTitle(ArrayList<Task> arr) {
		int pos ;
			pos = SearchTitle(arr);
		System.out.print("Do you want delete title true/ false ? : ");
		boolean check = ip.nextBoolean();
		if(check) {
			if(pos>=0) {
				arr.remove(pos);
				System.out.println("Delete Successful");
			}else {
				System.out.println("Delete Fail");
			}
		}else {
			System.out.println("Delete Fail");
		}

		
	}
	public void DeleteText(ArrayList<Task> arr) {

		int pos ;
		pos = SearchText(arr);
	System.out.print("Do you want delete text true/ false ? : ");
	boolean check = ip.nextBoolean();
	if(check) {
		if(pos>=0) {
			arr.remove(pos);
			System.out.println("Delete Successful");
		}else {
			System.out.println("Delete Fail");
		}
	}else {
		System.out.println("Delete Fail");
	}

		
	}
	public void DeleteAssignedTo(ArrayList<Task> arr) {
		int pos ;
		pos = SearchAssignedTo(arr);
	System.out.print("Do you want delete AssignedTo true/ false ? : ");
	boolean check = ip.nextBoolean();
	if(check) {
		if(pos>=0) {
			arr.remove(pos);
			System.out.println("Delete Successful");
		}else {
			System.out.println("Delete Fail");
		}
	}else {
		System.out.println("Delete Fail");
	}

	}
	
	public void SearchDisplayTitle(ArrayList<Task> arr) {
	 int pos = SearchTitle(arr);
	 if(pos>=0) {
		 System.out.println("The desired task is available at : "+arr.get(pos).getTitle());
	 }else {
		 System.out.println("Search Fail!");
	 }
	
	}
	public void SearchDisplayAssignedTo(ArrayList<Task> arr) {
		 int pos = SearchTitle(arr);
		 if(pos>=0) {
			 System.out.println("The desired task is available at : "+arr.get(pos).getAssignedTo());
		 }else {
			 System.out.println("Search Fail!");
		 }
		 
		
		}
	public void SearchDisplayText(ArrayList<Task> arr) {
		 int pos = SearchTitle(arr);
		 if(pos>=0) {
			 System.out.println("The desired task is available at : "+arr.get(pos).getText());
		 }else {
			 System.out.println("Search Fail!");
		 }
		
		}
}
